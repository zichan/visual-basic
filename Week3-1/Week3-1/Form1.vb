﻿Public Class Form1

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        Try
            If CInt(TextBox1.Text) > CInt(TextBox2.Text) Then
                MessageBox.Show("The left number is larger", "Number")
            ElseIf CInt(TextBox1.Text) < CInt(TextBox2.Text) Then
                MessageBox.Show("The right number is larger", "Number")
            Else
                MessageBox.Show("the two numbers are equal", "Number")
            End If
        Catch ex As Exception
            MessageBox.Show("You may have entered a letter as a number", "Error")
        End Try
    End Sub

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        TextBox1.Text = ""
        TextBox2.Text = ""
    End Sub

    Private Sub Button3_Click(sender As Object, e As EventArgs) Handles Button3.Click
        Me.Close()
    End Sub
End Class
