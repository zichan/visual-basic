﻿Public Class Form1

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        Dim fileStream As System.IO.StreamWriter
        If TextBox1.Text.Length > 0 And TextBox3.Text.Length > 0 Then
            Try
                fileStream = System.IO.File.CreateText(System.AppDomain.CurrentDomain.BaseDirectory + "\Employees\" + TextBox1.Text + TextBox3.Text + ".txt")
                fileStream.WriteLine(TextBox1.Text)
                fileStream.WriteLine(TextBox2.Text)
                fileStream.WriteLine(TextBox3.Text)
                fileStream.WriteLine(TextBox4.Text)
                fileStream.WriteLine(ComboBox1.SelectedIndex)
                fileStream.WriteLine(TextBox5.Text)
                fileStream.WriteLine(TextBox6.Text)
                fileStream.WriteLine(TextBox7.Text)
                fileStream.Close()
            Catch ex As Exception
                MessageBox.Show("There was an error with the file writing", "Error")
            End Try
        Else
            MessageBox.Show("Please enter atleast a first and last name", "Input Error")
        End If
    End Sub

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        TextBox1.Text = ""
        TextBox2.Text = ""
        TextBox3.Text = ""
        TextBox4.Text = ""
        ComboBox1.SelectedIndex = 0
        TextBox5.Text = ""
        TextBox6.Text = ""
        TextBox7.Text = ""
    End Sub

    Private Sub Button3_Click(sender As Object, e As EventArgs) Handles Button3.Click
        Me.Close()
    End Sub
End Class
