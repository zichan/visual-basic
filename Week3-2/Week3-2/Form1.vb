﻿Public Class Form1


    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        Try
            Dim first = CInt(TextBox1.Text)
            If first >= 1 And first <= 10 Then
                Select Case first
                    Case Is = 10
                        TextBox2.Text = "X"
                    Case Is = 9
                        TextBox2.Text = "IX"
                    Case Is = 8
                        TextBox2.Text = "VIII"
                    Case Is = 7
                        TextBox2.Text = "VII"
                    Case Is = 6
                        TextBox2.Text = "VI"
                    Case Is = 5
                        TextBox2.Text = "V"
                    Case Is = 4
                        TextBox2.Text = "IV"
                    Case Is = 3
                        TextBox2.Text = "III"
                    Case Is = 2
                        TextBox2.Text = "II"
                    Case Is = 1
                        TextBox2.Text = "I"
                End Select
            Else
                MessageBox.Show("Number needs to be larger than 0 and less than 11", "Error")
            End If
        Catch ex As Exception
            MessageBox.Show("You might have entered a letter as a number.", "Error")
        End Try
    End Sub

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        TextBox1.Text = ""
        TextBox2.Text = ""
    End Sub

    Private Sub Button3_Click(sender As Object, e As EventArgs) Handles Button3.Click
        Me.Close()
    End Sub
End Class
