﻿Public Class Input

    Public Sub New()

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.

    End Sub

    Protected Overrides Sub Finalize()
        MyBase.Finalize()
    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        Dim numb As Integer
        Try
            numb = CInt(TextBox1.Text)
        Catch ex As Exception
            MessageBox.Show("There was an error converting the text to an int.")
        End Try
        Dim total As Integer = 0
        For i As Integer = 1 To numb
            total += i
        Next
        MessageBox.Show("The total was " + total.ToString(), "Sum of numbers")
    End Sub

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        Me.Close()
    End Sub
End Class