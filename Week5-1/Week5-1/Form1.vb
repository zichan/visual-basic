﻿Public Class Form1

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        Try
            Dim cost As Integer = CInt(TextBox1.Text)
            Dim markup As Integer = CInt(TextBox2.Text)
            TextBox3.Text = CalculateRetail(cost, markup)
        Catch ex As Exception
            MessageBox.Show("One of the textboxes is not a number", "Error")
        End Try
    End Sub

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        Me.Close()
    End Sub
    Function CalculateRetail(ByVal cost As Integer, ByVal markup As Integer) As Decimal
        Return cost + (cost * (markup / 100))
    End Function
End Class
