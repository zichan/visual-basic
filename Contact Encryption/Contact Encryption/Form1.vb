﻿Imports System
Imports System.IO
Imports System.Text
Imports System.Security.Cryptography
'i used this guys code to learn how it works. http://www.obviex.com/samples/Encryption.aspx because i fly by dont reinvent the wheel
Public Class Form1
    Private enc As System.Text.UTF8Encoding = New System.Text.UTF8Encoding()
    Dim plainText As String = "moo, Mough"
    Dim cipherText As String
    Dim passPhrase As String = "telenough"
    Dim saltValue As String = "salt is a spice"
    Dim hashAlgorithm As String = "SHA1"
    Dim passwordIterations As Integer = 2
    Dim initVector As String = "@1B2c3D4e5F6g7H8" ' must be 16 bytes
    Dim keySize As Integer = 256                ' can be 192 or 128
    Private Sub Button1_Click(sender As Object, e As EventArgs)
        Dim sw As New StreamWriter("data\" + TextBox1.Text + ".txt")
        sw.Write(Encrypt(plainText, passPhrase, saltValue, hashAlgorithm, passwordIterations, initVector, keySize))
        sw.Close()
        Dim st As New StreamReader("data\" + TextBox1.Text + ".txt")
        Dim STR = Decrypt(st.ReadLine(), passPhrase, saltValue, hashAlgorithm, passwordIterations, initVector, keySize)
        Dim STRA() = Split(STR, ",")
        TextBox1.Text = STRA(0)
        TextBox2.Text = STRA(1)
    End Sub
    Public Shared Function Encrypt(ByVal plainText As String, ByVal passPhrase As String, ByVal saltValue As String, ByVal hashAlgorithm As String, ByVal passwordIterations As Integer, _
                                   ByVal initVector As String, ByVal keySize As Integer) As String

        ' Convert strings into byte arrays.
        ' Let us assume that strings only contain ASCII codes.
        ' If strings include Unicode characters, use Unicode, UTF7, or UTF8 
        ' encoding.
        Dim initVectorBytes As Byte()
        initVectorBytes = Encoding.ASCII.GetBytes(initVector)

        Dim saltValueBytes As Byte()
        saltValueBytes = Encoding.ASCII.GetBytes(saltValue)

        ' Convert our plaintext into a byte array.
        ' Let us assume that plaintext contains UTF8-encoded characters.
        Dim plainTextBytes As Byte()
        plainTextBytes = Encoding.UTF8.GetBytes(plainText)

        ' First, we must create a password, from which the key will be derived.
        ' This password will be generated from the specified passphrase and 
        ' salt value. The password will be created using the specified hash 
        ' algorithm. Password creation can be done in several iterations.
        Dim password As PasswordDeriveBytes
        password = New PasswordDeriveBytes(passPhrase, saltValueBytes, hashAlgorithm, passwordIterations)

        ' Use the password to generate pseudo-random bytes for the encryption
        ' key. Specify the size of the key in bytes (instead of bits).
        Dim keyBytes As Byte()
        keyBytes = password.GetBytes(keySize / 8)

        ' Create uninitialized Rijndael encryption object.
        Dim symmetricKey As RijndaelManaged
        symmetricKey = New RijndaelManaged()

        ' It is reasonable to set encryption mode to Cipher Block Chaining
        ' (CBC). Use default options for other symmetric key parameters.
        symmetricKey.Mode = CipherMode.CBC

        ' Generate encryptor from the existing key bytes and initialization 
        ' vector. Key size will be defined based on the number of the key 
        ' bytes.
        Dim encryptor As ICryptoTransform
        encryptor = symmetricKey.CreateEncryptor(keyBytes, initVectorBytes)
        ' Define memory stream which will be used to hold encrypted data.
        Dim memoryStream As MemoryStream
        memoryStream = New MemoryStream()
        ' Define cryptographic stream (always use Write mode for encryption).
        Dim cryptoStream As CryptoStream
        cryptoStream = New CryptoStream(memoryStream, _
                                        encryptor, _
                                        CryptoStreamMode.Write)
        ' Start encrypting.
        cryptoStream.Write(plainTextBytes, 0, plainTextBytes.Length)
        ' Finish encrypting.
        cryptoStream.FlushFinalBlock()
        ' Convert our encrypted data from a memory stream into a byte array.
        Dim cipherTextBytes As Byte()
        cipherTextBytes = memoryStream.ToArray()
        ' Close both streams.
        memoryStream.Close()
        cryptoStream.Close()
        ' Convert encrypted data into a base64-encoded string.
        Dim cipherText As String
        cipherText = Convert.ToBase64String(cipherTextBytes)
        ' Return encrypted string.
        Encrypt = cipherText
    End Function
    Public Shared Function Decrypt(ByVal cipherText As String, _
                                   ByVal passPhrase As String, _
                                   ByVal saltValue As String, _
                                   ByVal hashAlgorithm As String, _
                                   ByVal passwordIterations As Integer, _
                                   ByVal initVector As String, _
                                   ByVal keySize As Integer) _
                           As String

        ' Convert strings defining encryption key characteristics into byte
        ' arrays. Let us assume that strings only contain ASCII codes.
        ' If strings include Unicode characters, use Unicode, UTF7, or UTF8
        ' encoding.
        Dim initVectorBytes As Byte()
        initVectorBytes = Encoding.ASCII.GetBytes(initVector)

        Dim saltValueBytes As Byte()
        saltValueBytes = Encoding.ASCII.GetBytes(saltValue)

        ' Convert our ciphertext into a byte array.
        Dim cipherTextBytes As Byte()
        cipherTextBytes = Convert.FromBase64String(cipherText)

        ' First, we must create a password, from which the key will be 
        ' derived. This password will be generated from the specified 
        ' passphrase and salt value. The password will be created using
        ' the specified hash algorithm. Password creation can be done in
        ' several iterations.
        Dim password As PasswordDeriveBytes
        password = New PasswordDeriveBytes(passPhrase, _
                                           saltValueBytes, _
                                           hashAlgorithm, _
                                           passwordIterations)

        ' Use the password to generate pseudo-random bytes for the encryption
        ' key. Specify the size of the key in bytes (instead of bits).
        Dim keyBytes As Byte()
        keyBytes = password.GetBytes(keySize / 8)

        ' Create uninitialized Rijndael encryption object.
        Dim symmetricKey As RijndaelManaged
        symmetricKey = New RijndaelManaged()

        ' It is reasonable to set encryption mode to Cipher Block Chaining
        ' (CBC). Use default options for other symmetric key parameters.
        symmetricKey.Mode = CipherMode.CBC

        ' Generate decryptor from the existing key bytes and initialization 
        ' vector. Key size will be defined based on the number of the key 
        ' bytes.
        Dim decryptor As ICryptoTransform
        decryptor = symmetricKey.CreateDecryptor(keyBytes, initVectorBytes)

        ' Define memory stream which will be used to hold encrypted data.
        Dim memoryStream As MemoryStream
        memoryStream = New MemoryStream(cipherTextBytes)

        ' Define memory stream which will be used to hold encrypted data.
        Dim cryptoStream As CryptoStream
        cryptoStream = New CryptoStream(memoryStream, _
                                        decryptor, _
                                        CryptoStreamMode.Read)

        ' Since at this point we don't know what the size of decrypted data
        ' will be, allocate the buffer long enough to hold ciphertext;
        ' plaintext is never longer than ciphertext.
        Dim plainTextBytes As Byte()
        ReDim plainTextBytes(cipherTextBytes.Length)

        ' Start decrypting.
        Dim decryptedByteCount As Integer
        decryptedByteCount = cryptoStream.Read(plainTextBytes, _
                                               0, _
                                               plainTextBytes.Length)

        ' Close both streams.
        memoryStream.Close()
        cryptoStream.Close()

        ' Convert decrypted data into a string. 
        ' Let us assume that the original plaintext string was UTF8-encoded.
        Dim plainText As String
        plainText = Encoding.UTF8.GetString(plainTextBytes, _
                                            0, _
                                            decryptedByteCount)

        ' Return decrypted string.
        Decrypt = plainText
    End Function
    Private Function DecryptData(inName As String, rijnKey() As Byte, rijnIV() As Byte) As String
        Dim fin As New FileStream(inName, FileMode.Open, FileAccess.Read)
        'Create variables to help with read and write.
        Dim bin(100) As Byte 'This is intermediate storage for the encryption.
        Dim rdlen As Long = 0 'This is the total number of bytes written.
        Dim totlen As Long = fin.Length 'Total length of the input file.
        Dim len As Integer 'This is the number of bytes to be written at a time.
        'Creates the default implementation, which is RijndaelManaged.
        Dim rijn As SymmetricAlgorithm = SymmetricAlgorithm.Create()
        Dim encStream As New CryptoStream(fin, rijn.CreateEncryptor(rijnKey, rijnIV), CryptoStreamMode.Read)
        'Read from the input file, then encrypt and write to the output file.
        Dim Str
        Using reader As New StreamReader(encStream)
            Str = reader.Read()
        End Using
        encStream.Close()
        Return Str
    End Function

    Private Sub saveBTN_Click(sender As Object, e As EventArgs) Handles saveBTN.Click
        Dim sw As New StreamWriter("data\" + TextBox1.Text + TextBox2.Text + ".txt")
        plainText = TextBox1.Text + "," + TextBox2.Text + "," + TextBox3.Text + "," + TextBox4.Text + "," + TextBox5.Text + "," + TextBox6.Text + "," + TextBox7.Text + "," + TextBox8.Text + ","
        sw.Write(Encrypt(plainText, passPhrase, saltValue, hashAlgorithm, passwordIterations, initVector, keySize))
        sw.Close()
        saveBTN.Enabled = False
    End Sub

    Private Sub openBTN_Click(sender As Object, e As EventArgs) Handles openBTN.Click
        Dim fileLoc As String = OpenFileDialog1.ShowDialog()
        Dim st As New StreamReader(OpenFileDialog1.FileName)
        Dim STR = Decrypt(st.ReadLine(), passPhrase, saltValue, hashAlgorithm, passwordIterations, initVector, keySize)
        Dim STRA() = Split(STR, ",")
        TextBox1.Text = STRA(0)
        TextBox1.Enabled = False
        TextBox2.Text = STRA(1)
        TextBox2.Enabled = False
        TextBox3.Text = STRA(2)
        TextBox3.Enabled = False
        TextBox4.Text = STRA(3)
        TextBox4.Enabled = False
        TextBox5.Text = STRA(4)
        TextBox5.Enabled = False
        TextBox6.Text = STRA(5)
        TextBox6.Enabled = False
        TextBox7.Text = STRA(6)
        TextBox7.Enabled = False
        TextBox8.Text = STRA(7)
        TextBox8.Enabled = False
        editBtn.Enabled = True
        st.Close()
    End Sub

    Private Sub editBtn_Click(sender As Object, e As EventArgs) Handles editBtn.Click

        TextBox1.Enabled = True
        TextBox2.Enabled = True
        TextBox3.Enabled = True
        TextBox4.Enabled = True
        TextBox5.Enabled = True
        TextBox6.Enabled = True
        TextBox7.Enabled = True
        TextBox8.Enabled = True
    End Sub
End Class
