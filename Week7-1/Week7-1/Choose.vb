﻿Public Class Choose
    Private family As Boolean = False
    Private PhoneT, tax, Package, options As Decimal
    Private VMC As Integer = 5
    Private TMC As Integer = 10
    Public Sub showME(ByVal familyPlan As Boolean)
        family = familyPlan
        If family = True Then
            Me.Text = "Family Plan"
        Else
            Me.Text = "Individual Plan"
        End If
        Me.ShowDialog()
    End Sub
    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        If M100.Checked Then
            PhoneT = 29.95
        End If
        If M110.Checked Then
            PhoneT = 49.95
        End If
        If M200.Checked Then
            PhoneT = 99.95
        End If
        If VM.Checked Then
            options += VMC
        End If
        If TM.Checked Then
            options += TMC
        End If
        If MM300.Checked Then
            Package = 45
        End If
        If MM800.Checked Then
            Package = 65
        End If
        If MM1800.Checked Then
            Package = 99
        End If
        If family = True Then
            Dim number = CInt(InputBox("What is the number of phones you want?"))
            PhoneT *= number
        End If
        TextBox1.Text = PhoneT.ToString()
        TextBox2.Text = (PhoneT * 0.06).ToString()
        TextBox3.Text = (PhoneT + (PhoneT * 0.06)).ToString()
        TextBox4.Text = options.ToString()
        TextBox5.Text = Package.ToString()
        TextBox6.Text = (PhoneT.ToString() + (PhoneT + (PhoneT * 0.06)) + options + Package).ToString()
    End Sub

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        Me.Close()
    End Sub
End Class