﻿Public Class SpeedCalculator
    Public speed As Integer
    Public time As Integer

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        Dim inp As Input = New Input()
        inp.ShowDialog(Me)
        RichTextBox1.Text = ""
        Me.time = inp.time
        Me.speed = inp.speed
        Dim distarray(time) As Integer
        For i As Integer = 1 To time
            distarray(i) = speed * i
        Next
        RichTextBox1.AppendText("Vehicle speed: " & speed.ToString() & Environment.NewLine)
        RichTextBox1.AppendText("Time traveled: " & time.ToString() & Environment.NewLine)
        RichTextBox1.AppendText("Hour   Distance traveled" & Environment.NewLine)
        For i As Integer = 0 To time
            RichTextBox1.AppendText(i.ToString() & "   " & distarray(i).ToString() & Environment.NewLine)
        Next
        RichTextBox1.AppendText("Total distnace: " & distarray(time))
    End Sub

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        Me.Close()
    End Sub
End Class
